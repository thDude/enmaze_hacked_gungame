#define RELAY_PIN 2
#define BTN_PIN 3


void setup() {
  Serial.begin(9600);

  pinMode(RELAY_PIN, OUTPUT);
  pinMode(BTN_PIN, INPUT_PULLUP);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
}

void loop() {
  int val1 = analogRead(A1);
  int val2 = analogRead(A2);
  int val3 = analogRead(A3);

  int btn = digitalRead(BTN_PIN);

  Serial.print("BTN = ");
  Serial.print(btn);
  Serial.print("IR VCC = ");
  Serial.print(val1);
  Serial.print("  RD0 = ");
  Serial.print(val2);
  Serial.print("  LED+ = ");
  Serial.print(val3);
  Serial.println();


  delay(250);
}
