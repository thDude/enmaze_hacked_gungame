#define RELAY_PIN 2
#define BTN_PIN 3


boolean game_started = false;

int IR_VCC_val, BTN_val, RD0_val, GAME_LED_val;

int gameTimer = 0;
int GAME_hits = 0;
boolean wait_for_next_point = false;

void setup() {
  Serial.begin(9600);

  pinMode(RELAY_PIN, OUTPUT);
  pinMode(BTN_PIN, INPUT_PULLUP);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
}

void loop() {
  readValues();
  //debug2();

  if (game_started) {
    if (GAME_LED_val < 100) {
      GAME_hits++;
      wait_for_next_point = true;
      Serial.print("TARGET HIT! Score = ");
      Serial.println(GAME_hits);

      gameTimer = millis();
      delay(4000);
    }

    int time_now = millis();

    if (time_now - gameTimer > 28000 ) {
      //game over, too slow
      game_started = false;
      GAME_hits = 0;
      delay(1000);
      Serial.println("GAME OVER, too slow!");
    }
  }

  if (BTN_val && !game_started) {
    Serial.println("GAME STARTED");
    game_started = true;
    gameTimer = millis();
    startGame();    
  }


  if (GAME_hits >= 4) {
    //game won!
    Serial.println("GAME WON, congratulations!");
    game_started = false;
    GAME_hits = 0;
    delay(1000);
  }


  delay(50);
}


void startGame() {
  delay(50);
  digitalWrite(RELAY_PIN, HIGH);
  delay(250);
  digitalWrite(RELAY_PIN, LOW);
  delay(1500);
  digitalWrite(RELAY_PIN, HIGH);
  delay(250);
  digitalWrite(RELAY_PIN, LOW);
  delay(2500);
  Serial.println("GAME READY, START SHOOTING!");
}


void readValues() {
  IR_VCC_val = analogRead(A1);
  RD0_val = analogRead(A2);
  GAME_LED_val = analogRead(A3);

  BTN_val = digitalRead(BTN_PIN);
}

void debug() {
  Serial.print("BTN = ");
  Serial.print(BTN_val);
  Serial.print(";  IR VCC = ");
  Serial.print(IR_VCC_val);
  Serial.print(";  RD0 = ");
  Serial.print(RD0_val);
  Serial.print(";  LED+ = ");
  Serial.print(GAME_LED_val);
  Serial.println();
}

void debug2() {
  Serial.print("game Started = ");
  Serial.print(game_started);
  Serial.print(";  HITS = ");
  Serial.print(GAME_hits);
  Serial.println();
}

