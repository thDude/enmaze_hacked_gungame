#include "FastLED.h"

#define LED_PIN 5
#define NUM_LEDS 8

CRGB leds[NUM_LEDS];
#define BRIGHTNESS 250
int gHue = 0;

#define RELAY_PIN 2
#define BTN_PIN 3


boolean game_started = false;

int IR_VCC_val, BTN_val, RD0_val, GAME_LED_val;

int gameTimer = 0;
int GAME_hits = 0;
boolean wait_for_next_point = false;

void setup() {
  delay(500);
  Serial.begin(9600);

  FastLED.addLeds<NEOPIXEL, LED_PIN>(leds, 0, 8);
  FastLED.setBrightness(BRIGHTNESS);

  pinMode(RELAY_PIN, OUTPUT);
  pinMode(BTN_PIN, INPUT_PULLUP);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);


  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i].setRGB(0, 0, 0);
  }
  FastLED.show();
}

void loop() {
  readValues();
  debug();

  if (game_started) {
    if (!BTN_val) {
      //Serial.println("game is running");
      leds_ready_to_shoot();
      if (GAME_LED_val < 100) {
        GAME_hits++;
        leds_show_points();
        gameTimer = millis();

        Serial.print("TARGET HIT! Score = ");
        Serial.println(GAME_hits);

        if (GAME_hits < 5) {
          leds_clear();
          FastLED.show();
          delay(6000);
          Serial.println("Ready for next Shot!");
          leds_ready_to_shoot();
        }
      }

      int time_now = millis();

      if (time_now - gameTimer > 28000 ) {
        //game over, too slow
        game_started = false;
        GAME_hits = 0;
        leds_clear();

        Serial.println("GAME OVER, too slow!");

        delay(1000);
      }
    } else {
      Serial.println("GAME OVER, you let go of the Button!");
      game_started = false;
      GAME_hits = 0;
      leds_clear_all();
      stopGame();      
    }
  }

  if (!BTN_val && !game_started) {
    Serial.println("GAME STARTED");
    game_started = true;
    gameTimer = millis();
    startGame();
  }


  if (GAME_hits >= 5) {
    //game won!
    Serial.println("GAME WON, congratulations!");
    game_started = false;
    GAME_hits = 0;
    leds_game_won();
    delay(1000);
  }

  delay(50);
}


void startGame() {
  delay(50);
  digitalWrite(RELAY_PIN, HIGH);
  delay(250);
  digitalWrite(RELAY_PIN, LOW);
  delay(1500);
  digitalWrite(RELAY_PIN, HIGH);
  delay(250);
  digitalWrite(RELAY_PIN, LOW);
  delay(2000);
  Serial.println("GAME READY, START SHOOTING!");
}

void stopGame(){
  delay(50);
  digitalWrite(RELAY_PIN, HIGH);
  delay(250);
  digitalWrite(RELAY_PIN, LOW);
  delay(1500);
}


void readValues() {
  IR_VCC_val = analogRead(A1);
  RD0_val = analogRead(A2);
  GAME_LED_val = analogRead(A3);

  BTN_val = digitalRead(BTN_PIN);
}

void leds_ready_to_shoot() {
  for (int i = 4; i < NUM_LEDS; i++) {
    leds[i].setRGB(100, 100, 100);
  }
  FastLED.show();
}

void leds_clear() {
  for (int i = 4; i < NUM_LEDS; i++) {
    leds[i].setRGB(0, 0, 0);
  }
  FastLED.show();
}

void leds_clear_all() {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i].setRGB(0, 0, 0);
  }
  FastLED.show();
}

void leds_show_points() {
  for (int i = 1; i <= GAME_hits; i++) {
    leds[i - 1].setRGB(0, 255, 0);
  }
  FastLED.show();
}

void leds_game_won() {

  for (int i = 0; i < 3000; i++) {
    fill_rainbow( leds, NUM_LEDS, gHue, 7);
    gHue++;
    delay(2);
    FastLED.show();
  }
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i].setRGB(0, 0, 0);
  }
  FastLED.show();
}

void debug() {
  Serial.print("BTN = ");
  Serial.print(BTN_val);
  Serial.print(";  IR VCC = ");
  Serial.print(IR_VCC_val);
  Serial.print(";  RD0 = ");
  Serial.print(RD0_val);
  Serial.print(";  LED+ = ");
  Serial.print(GAME_LED_val);
  Serial.println();
}

void debug2() {
  Serial.print("game Started = ");
  Serial.print(game_started);
  Serial.print(";  HITS = ");
  Serial.print(GAME_hits);
  Serial.println();
}

