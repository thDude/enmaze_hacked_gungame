#include "FastLED.h"

#define LED_PIN 5
#define NUM_LEDS 8

CRGB leds[NUM_LEDS];
#define BRIGHTNESS 250
int gHue = 0;

#define RELAY_PIN 2
#define BTN_PIN 3
#define LIGHT_PIN 6
#define ONOFF_PIN 4
#define MAIN_SWITCH 7
#define QUEEN_PIN 8

boolean game_started = false;

int IR_VCC_val, BTN_val, RD0_val, GAME_LED_val;

int gameTimer = 0;
int GAME_hits = 0;
boolean wait_for_next_point = false;
boolean bootup = false;
boolean system_running = true;
boolean game_won = false;

void setup() {
  delay(500);
  Serial.begin(9600);

  FastLED.addLeds<NEOPIXEL, LED_PIN>(leds, 0, 8);
  FastLED.setBrightness(BRIGHTNESS);

  pinMode(QUEEN_PIN, OUTPUT);
  pinMode(RELAY_PIN, OUTPUT);
  pinMode(ONOFF_PIN, OUTPUT);
  pinMode(LIGHT_PIN, OUTPUT);
  pinMode(BTN_PIN, INPUT_PULLUP);
  pinMode(MAIN_SWITCH, INPUT_PULLUP);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);

  digitalWrite(QUEEN_PIN, LOW);


  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i].setRGB(0, 0, 0);
  }
  FastLED.show();

  //attachInterrupt(digitalPinToInterrupt(BTN_PIN), interupt_stop, RISING);
}
boolean justScored = false;

void loop() {
  readValues();
  if (!system_running) {
    digitalWrite(ONOFF_PIN, LOW);
    bootup = false;
    game_started = false;
    GAME_hits = 0;
    leds_clear();
    digitalWrite(LIGHT_PIN, LOW);
    FastLED.show();
    Serial.println("System OFF!");
    game_won = false;
    return;
  }
  bootup_clock();
  debug();

  if (game_started && !game_won) {
    if (!BTN_val) {
      //Serial.println("game is running");
      if(GAME_hits == 0) leds_ready_to_shoot();
      if (GAME_LED_val < 100 && !justScored) {
        justScored = true;
        GAME_hits++;
        leds_show_points();
        gameTimer = millis();
        leds_clear();
        FastLED.show();
        delay(4000);
        Serial.print("TARGET HIT! Score = ");
        Serial.println(GAME_hits);
      }
      if (justScored) {
        //wait for signal for next round
        if (GAME_LED_val < 100) {
          leds_clear();
          FastLED.show();
          Serial.println("waiting for next Shot!");
          delay(50);
        } else {
          justScored = false;
          if (GAME_hits < 5) {

            Serial.println("Ready for next Shot!");
            leds_ready_to_shoot();
          }
        }
      }


      int time_now = millis();

      int timeToFinish  = (GAME_hits > 0) ? 23000 : 29000; //first round has a 30 secs timeout, all others 20 secs

      if (time_now - gameTimer >  timeToFinish) {
        //game over, too slow
        timeout();
      }
    } else {
      Serial.println("GAME OVER, you let go of the Button!");
      digitalWrite(LIGHT_PIN, LOW);
      game_started = false;
      GAME_hits = 0;
      leds_clear_all();
      stopGame();
    }
  }

  if (!BTN_val && !game_started && !game_won) {
    Serial.println("GAME STARTED");
    game_started = true;
    gameTimer = millis();
    startGame();
  }


  if (GAME_hits >= 5) {
    //game won!
    Serial.println("GAME WON, congratulations!");
    game_started = false;
    GAME_hits = 0;
    leds_game_won();
    game_won = true;
    delay(1000);
  }

  if (game_won) {
    digitalWrite(QUEEN_PIN, HIGH);
  } else {
    digitalWrite(QUEEN_PIN, LOW);
  }

  delay(50);
}

void timeout() {
  game_started = false;
  GAME_hits = 0;
  leds_clear_all();
  digitalWrite(LIGHT_PIN, LOW);
  Serial.println("GAME OVER, too slow!");
  //reset_device(); dont reset here, its buggy!
  delay(4000);
}

void bootup_clock() {
  if (!bootup) {
    digitalWrite(ONOFF_PIN, HIGH);
    bootup = true;
    delay(1000);
  } else {
    digitalWrite(LIGHT_PIN, HIGH);
  }
}

void startGame() {
  delay(50);
  digitalWrite(RELAY_PIN, HIGH);
  delay(250);
  digitalWrite(RELAY_PIN, LOW);
  delay(1500);
  digitalWrite(RELAY_PIN, HIGH);
  delay(250);
  digitalWrite(RELAY_PIN, LOW);
  delay(2000);
  Serial.println("GAME READY, START SHOOTING!");
}


void interupt_stop() {
  Serial.println("INTERRUPT!");
  stopGame();
}

void stopGame() {
  delay(50);
  digitalWrite(RELAY_PIN, HIGH);
  delay(250);
  digitalWrite(RELAY_PIN, LOW);
  delay(2700);
  digitalWrite(ONOFF_PIN, LOW);
  bootup = false;
  delay(1000);
}

void reset_device() {
  delay(2000);
  digitalWrite(ONOFF_PIN, LOW);
  bootup = false;
}


void readValues() {
  system_running = digitalRead(MAIN_SWITCH);
  IR_VCC_val = analogRead(A1);
  RD0_val = analogRead(A2);
  GAME_LED_val = analogRead(A3);

  BTN_val = digitalRead(BTN_PIN);
}

void leds_ready_to_shoot() {
  for (int i = 4; i < NUM_LEDS; i++) {
    leds[i].setRGB(100, 100, 100);
  }
  FastLED.show();
}

void leds_clear() {
  for (int i = 4; i < NUM_LEDS; i++) {
    leds[i].setRGB(0, 0, 0);
  }
  FastLED.show();
}

void leds_clear_all() {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i].setRGB(0, 0, 0);
  }
  FastLED.show();
}

void leds_show_points() {
  for (int i = 1; i <= GAME_hits; i++) {
    leds[i - 1].setRGB(0, 255, 0);
  }
  FastLED.show();
}

void leds_game_won() {

  for (int i = 0; i < 3000; i++) {
    fill_rainbow( leds, NUM_LEDS, gHue, 7);
    gHue++;
    delay(2);
    FastLED.show();
  }
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i].setRGB(0, 0, 0);
  }
  FastLED.show();
}

void debug() {
  Serial.print("BTN = ");
  Serial.print(BTN_val);
  Serial.print(";  IR VCC = ");
  Serial.print(IR_VCC_val);
  Serial.print(";  RD0 = ");
  Serial.print(RD0_val);
  Serial.print(";  LED+ = ");
  Serial.print(GAME_LED_val);
  Serial.println();
}

void debug2() {
  Serial.print("game Started = ");
  Serial.print(game_started);
  Serial.print(";  HITS = ");
  Serial.print(GAME_hits);
  Serial.println();
}

